import random

class Rule:
    def __init__(self, condition, actions) -> None:
        self.condition = condition
        self.actions = actions

    def get_condition(self):
        return self.condition

    def get_actions(self):
        return self.actions

    def get_action(self):
        return random.choice(self.actions)

interpret_input_dict = {
    "hello": "greet",
    "good morning": "greet",
    "bye": "farewel",
    "hungry": "eat",
    "anything new": "check-email"
}

rules = [
    Rule("greet", ["Hello", "Hi", "Good morning"]),
    Rule("farewel", ["Good bye", "See you"]),
    Rule("eat", ["do you want me to order you some food?", "Drink some water", "get yourself some fruits"]),
    Rule("check-email", ["Nope, you caught up with all the email", "You have one unread email", "You have one to do list left", "you have one assignment left"]),
    Rule("_unknown_state_", ["Sorry, I can't response to this"]),
]

def interpret_input(user_input):
    if user_input in interpret_input_dict:
        return interpret_input_dict[user_input]
    else:
        return "_unknown_state_"

def rule_match(state, rules):
    for rule in rules:
        if rule.get_condition() == state:
            return rule
    return None

def simple_reflex_agent(perception):
    state = interpret_input(perception)
    rule = rule_match(state, rules)
    print("rule", rule)
    action = rule.get_action()
    return action

while True:
    perception = input("> ")
    action = simple_reflex_agent(perception)
    print(action)




